package com.example.rabbittest.domain;

/**
 * Created by Vitali_Bliakharchuk on 3/22/2018.
 */
public enum Severity {

    ERROR("error"), WARN("warn"), DEBUG("debug"), INFO("info");

    private String value;

    Severity(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
