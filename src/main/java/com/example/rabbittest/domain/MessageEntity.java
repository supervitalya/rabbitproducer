package com.example.rabbittest.domain;

import lombok.Data;

/**
 * Created by Vitali_Bliakharchuk on 3/21/2018.
 */
@Data
public class MessageEntity {

    private String messageText;
    private String severity;

}
