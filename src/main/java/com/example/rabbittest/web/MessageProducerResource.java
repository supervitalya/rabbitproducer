package com.example.rabbittest.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.rabbittest.domain.MessageEntity;
import com.example.rabbittest.service.RabbitProducerService;
import lombok.extern.java.Log;

/**
 * Created by Vitali_Bliakharchuk on 3/21/2018.
 */
@CrossOrigin
@RestController("/messages")
@Log
public class MessageProducerResource {

    private final RabbitProducerService rabbitProducerService;

    public MessageProducerResource(RabbitProducerService rabbitProducerService) {
        this.rabbitProducerService = rabbitProducerService;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void publishMessage(@RequestBody MessageEntity message) {
        log.info("Message producer resource received message: " + message);
        rabbitProducerService.publishMessage(message);
    }
}
