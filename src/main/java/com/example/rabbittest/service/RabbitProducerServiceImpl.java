package com.example.rabbittest.service;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.rabbittest.domain.MessageEntity;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import lombok.extern.java.Log;

/**
 * Created by Vitali_Bliakharchuk on 3/21/2018.
 */
@Log
@Service
public class RabbitProducerServiceImpl implements RabbitProducerService {

    @Value("${app.exchangeType}")
    private String exchangeType;

    @Value("${app.exchangeName}")
    private String exchangeName;

    @Value("${app.host}")
    private String host;

    @Override
    public void publishMessage(MessageEntity message) {
        log.info(String.format("Received message %s.", message.toString()));
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            channel.exchangeDeclare(exchangeName, exchangeType);
            channel.basicPublish(exchangeName, message.getSeverity(), MessageProperties.PERSISTENT_TEXT_PLAIN, message.getMessageText().getBytes());

            log.info(String.format("Message: %s has been published", message.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
