package com.example.rabbittest.service;

import com.example.rabbittest.domain.MessageEntity;

/**
 * Created by Vitali_Bliakharchuk on 3/21/2018.
 */
public interface RabbitProducerService {

    void publishMessage(MessageEntity message);
}
